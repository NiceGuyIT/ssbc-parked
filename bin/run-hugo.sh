#!/usr/bin/env bash

# Hugo does not clean out the public/ directory when content is moved or deleted.
# https://github.com/gohugoio/hugo/issues/2389

BASE_DIR="$( dirname "$( dirname "$( readlink --canonicalize "${BASH_SOURCE[0]}" )" )" )"
HUGO="/usr/local/bin/hugo"

# Hugo puts the public directory in the current directory
cd "${BASE_DIR}" || exit

#	--liveReloadPort=11111 \
"${HUGO}" server \
	--renderToDisk \
	--disableFastRender \
	--appendPort=false \
	--baseUrl=http://localhost:1330 \
	--port=1330 \
	--verbose

