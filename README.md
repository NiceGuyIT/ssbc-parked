# maintenance-parked
This is the business closure website for [Smith Small Business Consultants, LLC](https://SmithSmallBusinessConsultants.com).


## Theme
This Hugo site uses the [Highlights](https://github.com/schmanat/hugo-highlights-theme) theme.

```bash
cd themes
git clone https://github.com/schmanat/hugo-highlights-theme
```

